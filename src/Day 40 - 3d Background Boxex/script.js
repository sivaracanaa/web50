const magicBtn = document.getElementById('btn')
const boxesContainer = document.getElementById('boxes')

let noOfBoxes = 4

addBoxes()

magicBtn.addEventListener('click', () => boxesContainer.classList.toggle('big'))

function addBoxes() {
    let xVal = 0
    let yVal = 0

    for(let i = 0; i < noOfBoxes; i++){
        for(let j = 0; j < noOfBoxes; j++){
            const box = createBox()

            box.style.backgroundPosition = `${-xVal}px ${-yVal}px`
            
            xVal += 125

            boxesContainer.appendChild(box)
        }
        xVal = 0
        yVal += 125
    }
}

function createBox(){
    const box = document.createElement('div')
    box.classList.add('box')
    return box
}

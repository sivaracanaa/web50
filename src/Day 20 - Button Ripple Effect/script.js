const buttons = document.querySelectorAll('.ripple')

buttons.forEach(button => {
    button.addEventListener('click', event => {
        let x = event.clientX
        let y = event.clientY
        
        let btnTop = event.target.offsetTop
        let btnLeft = event.target.offsetLeft
        
        let xInside = x - btnLeft
        let yInside = y - btnTop
        
        let circleEl = document.createElement('span')
        circleEl.classList.add('circle')
        circleEl.style.left = xInside + 'px'
        circleEl.style.top = yInside + 'px'
        button.appendChild(circleEl)
        setTimeout(() => circleEl.remove(), 500)
    })
})

// get the needed items from the DOM
const images = document.querySelectorAll('.content')
const navs = document.querySelectorAll('nav li')

// add event listners to all the nav items
//   * use index argument to get the exact image to which 
//     the show class is to be added
navs.forEach((nav, idx) => {
    nav.addEventListener('click', () => {
        // first remove show class from all the content
        hideAllContents()
        // and then add show class to the needed content
        images[idx].classList.add('show')
        
        // remove active class from all the navs
        hideAllItems()
        // ad then add the active class to the needed nav item
        nav.classList.add('active')
    })
})

// functions to remove the needed classes
function hideAllContents() {
    images.forEach(img => img.classList.remove('show'))
}

function hideAllItems(){
    navs.forEach(nav => nav.classList.remove('active'))
}

// get the needed elements form the dom
const image = document.querySelector('div.loveMe')
const counter = document.getElementById('times')

// counter needed to tell how many times the user has clicked
let count = 0

// this is used to check if the click is double click or not
let clickTime = 0

// the event listner could use 'dblclick' event also but to make it more chalenging it checks if there is a second click in 800 ms
image.addEventListener('click', event => {
    if (clickTime === 0){
        clickTime = new Date().getTime() // get the time at which first click hapened
    } else {
        if ((new Date().getTime() - clickTime) < 800){
            createHeart(event) // if clicked in 800 ms create a heart and add 1 to the counter
            count++
            counter.innerHTML = count
            clickTime = 0 // reset timer
        } else {
            // if not clicked in 800 create new event start
            clickTime = new Date().getTime()
        }
    }
})

function createHeart(event){
    // create a <i class="fas fa-heart"></i> for heart
    let heart = document.createElement('i')

    // add needed classes to the heart
    heart.classList.add('fas')
    heart.classList.add('fa-heart')

    // give the heart the position of click
    // there is a different way also using event.clientX but i think this is simple and takes a lot less line of code
    heart.style.top = `${event.offsetY}px`
    heart.style.left = `${event.offsetX}px`

    // insert the heart and to prevent cluttering of elements in image remove it after 1s
    image.appendChild(heart)
    setTimeout(()=>heart.remove(), 1000)
}

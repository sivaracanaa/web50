const header = document.getElementById('header')
const title = document.getElementById('title')
const exerpt = document.getElementById('exerpt')
const profile_img = document.getElementById('profile_img')
const _name = document.getElementById('name')
const date = document.getElementById('date')

const animated_bgs = document.querySelectorAll('.animated-bg')
const animated_bgs_text = document.querySelectorAll('.animated-bg-text')

function getData() {
    header.innerHTML = '<img src="https://images.unsplash.com/photo-1496181133206-80ce9b88a853?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2102&q=80" alt="image">'
    title.innerHTML = 'Lorem ipsum dolor sit amet.'
    exerpt.innerHTML = 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi, qui?'
    profile_img.innerHTML = '<img src="https://randomuser.me/api/portraits/men/45.jpg" alt="">'
    _name.innerHTML = 'John Doe'
    date.innerHTML = 'Feb 17, 2021'
    animated_bgs.forEach(element => {
        element.classList.remove('animated-bg')
    })
    animated_bgs_text.forEach(element => {
        element.classList.remove('animated-bg-text')
    })
}

setInterval(getData, 2500);

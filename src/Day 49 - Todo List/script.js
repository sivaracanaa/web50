// the needed DOM elements
const input = document.getElementById('input')
const todos = document.getElementById('todos')
const form = document.getElementById('form')

// check if there are todos in the local storage
const todoData = JSON.parse(localStorage.getItem('todos'))

// if todos is local storage put them in the DOM
if(todoData){
    todoData.forEach(todo => addTodo(todo.text, todo.completed));
}

// create a new todo on form submitted
form.addEventListener('submit', event => {
    event.preventDefault()

    if (input.value.trim() != ''){
        addTodo(input.value.trim())
    }
})

// function which creates a todo in the DOM
function addTodo(todo, completed = false) {
    const todoEl = document.createElement('li')
    
    todoEl.innerHTML = todo

    if (completed) {
        todoEl.classList.add('completed')
    }

    // left click event lisner
    todoEl.addEventListener('click',() => {
        todoEl.classList.toggle('completed')
        updateLS()
    })

    // right click event listners
    todoEl.addEventListener('contextmenu', event => {
        event.preventDefault()
        todoEl.remove()
        updateLS()
    })

    // put the todo in the DOM and clear the input
    todos.appendChild(todoEl)
    input.value = ''
}

// Update the local storage 
function updateLS() {
    // get all the todo from the DOM
    const todoEls = document.querySelectorAll('.todos li')

    let todos = []

    // get the important data of the todo and put it into local storage
    for(const todo of todoEls){
        todos.push({
            text: todo.innerHTML,
            completed: todo.classList.contains('completed')
        })
    }

    // save into the loacl storage
    localStorage.setItem('todos', JSON.stringify(todos))
}

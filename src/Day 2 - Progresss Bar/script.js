const progress = document.querySelector('#progress')
const prev = document.querySelector('#prev')
const next = document.querySelector('#next')
const circles = document.querySelectorAll('.circle')

let currentActive = 1

next.addEventListener('click', () => {
    currentActive++
    prev.disabled = false
    if (currentActive >= circles.length){
        next.disabled = true
    }

    update()
})

prev.addEventListener('click', () => {
    currentActive--
    next.disabled = false
    if (currentActive <= 1){
        prev.disabled = true
    }

    update()
})

const update = () => {
    circles.forEach((circle, index) => {
        if (index < currentActive){
            circle.classList.add('active')
        } else {
            circle.classList.remove('active')
        }
    })

    let actives = document.querySelectorAll('.active')
    let len = ((actives.length-1)/(circles.length-1))*100
    progress.style.width = len + '%'
}

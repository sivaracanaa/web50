const nav = document.querySelector('.nav')
window.addEventListener('scroll', fixNav)

function fixNav(event){
    if (window.scrollY > 20){
        nav.classList.add('active')
    } else {
        nav.classList.remove('active')
    }
}

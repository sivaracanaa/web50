const container = document.getElementById('container')
const colors = ['#e74c3c', '#8e44ad', '#3498db', '#e67e22', '#2ecc71'] // colors
const SQUARES = 400

// Crteating the squares
for(let i = 0; i < SQUARES; i++){
    addSquare()
}

function addSquare() {
    const square = document.createElement('div')
    square.classList.add('square') // make it a square

    // add the necessary event listners
    square.addEventListener('mouseover', () => setColor(square))

    square.addEventListener('mouseout', () => removeColor(square))

    // Dont forget to add it to the DOM
    container.appendChild(square)
}

function setColor(square){
    const color = getRandomColor()

    // set the background the random color
    square.style.background = color
    
    // the nice boxshadow to make it glow
    square.style.boxShadow = `0 0 2px ${color}, 0 0 10px ${color}`
}

function removeColor(square) {
    // set the square back to default
    square.style.background = '#1D1D1D'
    square.style.boxShadow = '0 0 2px #000'
}

function getRandomColor(){
    // use Math.Random() to generate a random index 
    return colors[Math.floor(Math.random() * colors.length)]
}

// Get the needed DOM elements 
const openBtn = document.querySelector('.open-btn')
const closeBtn = document.querySelector('.close-btn')

const navs = document.querySelectorAll('.nav')

// Event listners to open and close the navigation plane
openBtn.addEventListener('click', () => {
    navs.forEach(nav => nav.classList.add('visible'))
})

closeBtn.addEventListener('click', () => {
    navs.forEach(nav => nav.classList.remove('visible'))
})

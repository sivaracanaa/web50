const smallCups = document.querySelectorAll('.cup.cup-small')
const liters = document.getElementById('liters')
const percentage = document.getElementById('percentage')
const remained = document.getElementById('remained')

updateBigCup()

smallCups.forEach((cup, idx) => {
    cup.addEventListener('click', e => {
        highlightCups(idx)
    })
})

function highlightCups(idx) {
    if (smallCups[idx].classList.contains('full') && !smallCups[idx].nextElementSibling.classList.contains('full')){
        idx--
    }

    smallCups.forEach((cup, ind) => {
        if (ind <= idx){
            cup.classList.add('full')
        }else{
            cup.classList.remove('full')
        }
    })

    updateBigCup()
}

function updateBigCup() {
    const fullCups = document.querySelectorAll('.cup.cup-small.full').length
    const totalCups = smallCups.length

    console.log(fullCups, totalCups)
    if(fullCups === 0){
        percentage.innerText = ''
        percentage.style.height = 0
    } else {
        percentage.innerText = `${fullCups/totalCups*100}%`
        percentage.style.height = `${fullCups/totalCups*330}px`
    }

    if (fullCups === totalCups) {
        remained.style.visibility = 'hidden'
        remained.style.height = 0
    } else {
        remained.style.visibility = 'visible'
        liters.innerText = `${2 - (250 * fullCups / 1000)} L`
    }
}

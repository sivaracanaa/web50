const codes = document.querySelectorAll('.code')

// when the page loaded first input should be in the foucs
codes[0].focus()

// handle if the user entered any number
codes.forEach((code, idx) => {
    code.addEventListener('keydown', (e) => {
        if(e.key >= 0 && e.key <=9) {
            code.value = '' // to avoid the extra number when entered it twice
            setTimeout(() => codes[idx + 1].focus(), 10)
        } else if(e.key === 'Backspace') {
            // handle the backspace event
            setTimeout(() => codes[idx - 1].focus(), 10)
        }
    })
})
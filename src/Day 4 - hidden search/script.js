const search_btn = document.querySelector('.btn')
const input_box = document.querySelector('.input')
const search_widget = document.querySelector('.search')

search_btn.addEventListener('click', () => {
    search_widget.classList.toggle('active')
    input_box.focus()
})

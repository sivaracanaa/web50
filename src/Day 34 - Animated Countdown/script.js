const nums = document.querySelector('.nums span')
const counterEl = document.querySelector('.counter')
const finalEl = document.querySelector('.final')
const replayBtn = document.getElementById('replay')

const countdownMilliseconds = 600

let inEl = document.querySelector('span.in')

replayCountdown()

replayBtn.addEventListener('click', replayCountdown)

function replayCountdown(){
    counterEl.classList.remove('hide')
    finalEl.classList.remove('show')

    function countdown() {
        inEl.classList.remove('in')
        inEl.classList.add('out')

        if (inEl.nextElementSibling == null){
            setTimeout(() => {}, countdownMilliseconds)
            counterEl.classList.add('hide')
            finalEl.classList.add('show')

            nums.classList.add('in')
            inEl = nums

            return
        }

        inEl = inEl.nextElementSibling
        inEl.classList.remove('out') 
        setTimeout(countdown, countdownMilliseconds)
    }

    setTimeout(countdown, countdownMilliseconds)
}
